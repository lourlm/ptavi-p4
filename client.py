#!/usr/bin/python3
"""
Programa cliente UDP que abre un socket a un servidor
"""


import socket
import sys
#   SOCKET: caminos virtuales que abrimos entre maquinas
#   shocket está atado a un ip y un puerto en ambos lados

# Constantes. Dirección IP del servidor y contenido a enviar
SERVER = sys.argv[1]
PORT = int(sys.argv[2])
LINE = str(sys.argv[3:])
LINE = ' '.join(sys.argv[3:])
SIP = sys.argv[4]
EXPIRES = sys.argv[5]

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    request = 'REGISTER sip: ' + str(SIP) + " SIP/2.0r" + " EXPIRES: " + EXPIRES
    my_socket.connect((SERVER, PORT))  # listas fijas
    print("Enviando:", request)
    my_socket.send(bytes(request, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)
    print('Recibido -- ', data.decode('utf-8'))  # de byte a utf-8

print("Socket terminado.")
