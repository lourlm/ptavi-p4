#!/usr/bin/python3
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import json
import time


class SipRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """

    diccionario = {}

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """

        self.json2registered()
        self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
        IpClient = self.client_address[0]
        PortClient = self.client_address[1]

        for line in self.rfile:
            print("El cliente con la IP: " + str(IpClient))
            print("con el puerto: " + str(PortClient) + ". ")
            print("Nos ha enviado:", line.decode('utf-8'))

            petition = line.decode('utf-8').split()

            direction = line.decode('utf-8').split(':')[1]
            direction = direction.split('SIP')[-2]
            client = self.client_address[0]
            expires = int(petition[5])
            Time = time.time() + expires
            TimeExpires = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(Time))
            TimeNow = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(time.time()))

            if petition[0] == 'REGISTER':
                self.diccionario[direction] = [client, TimeExpires, time.timezone]
                print("Sus datos: ", self.client_address)

                if petition[4] == 'EXPIRES:':
                    if expires == 0:
                        del self.diccionario[direction]

                    for usuarios in self.diccionario.copy():
                        if TimeNow >= self.diccionario[usuarios][1]:
                            del self.diccionario[usuarios]

                    else:
                        print("Guardamos su registro")

            else:
                print("Usage: client.py ip puerto register sup_address expires_value")

            print(self.diccionario)
            self.register2json()

    def register2json(self):
        """
        Abrimos el fichero y escribimos en el lo del diccionario.
        """
        with open('registered.json', 'w') as file:
            json.dump(self.diccionario, file)

    def json2registered(self):
        """
        Abrimos el fichero y leemos en el para comprobar si existe.
        """
        try:
            with open('registered.json', 'r') as newfile:
                self.diccionario = json.load(newfile)
        except:
            pass
            print("No existe")


if __name__ == "__main__":
    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request

    PORT = int(sys.argv[1])
    serv = socketserver.UDPServer(('', PORT), SipRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
